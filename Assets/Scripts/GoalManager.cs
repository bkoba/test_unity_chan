﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GoalManager : MonoBehaviour
{
    // NOTE publicの場合、Unityエディタ上でアタッチされたオブジェクトから操作できる
    // ユニティちゃんを格納する
    public GameObject player;
    // テキストを格納する
    public GameObject text;

    private bool isGoal = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // ゴールした後で画面をクリックされたときリスタートする
        if (isGoal && Input.GetMouseButton(0))
        {
            Restart();
        }
    }

    // 当たり判定 物理同士を透過させたい場合用 (IsTrigger=Trueのオブジェクトでのみ呼び出される)
    // 他のオブジェクトが接触した瞬間に呼び出される
    // other: 接触してきたオブジェクト
    private void OnTriggerEnter(Collider other)
    {
        if (other.name == player.name)
        {
            UnityEngine.Debug.Log("ユニティちゃんと接触しました！");

            // テキストの内容を変更する
            text.GetComponent<Text>().text = "Goal!!!\n画面クリックで再スタート";
            // テキストを表示する (gameObjectを有効にする)
            text.SetActive(true);
            // NOTE コンポーネントの有効無効を切り替える時はenabled変数を変更する

            isGoal = true;
        }
    }

    // シーンの再読み込み
    private void Restart()
    {
        // 現在のSceneを取得
        Scene loadScene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(loadScene.name);
    }
}
