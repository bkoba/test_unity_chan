﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityChan;

public class DeadWallController : MonoBehaviour
{
    // オブジェクトの速度
    public float speed = 0.05f;
    public float max_x = 10.0f;

    // ユニティちゃんを格納する変数
    public GameObject player;
    // テキストを格納する変数
    public GameObject text;

    // ゲームオーバー判定
    private bool isGameOver = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // フレーム毎speed分だけx軸方向に移動する
        this.gameObject.transform.Translate(speed, 0, 0);

        // Transformのxが一定値を超えたときに向きを反対にする
        float x = this.gameObject.transform.position.x;
        if (x > max_x || x < (-max_x))
        {
            speed *= -1;
        }

        // ゲームオーバー状態で画面がクリックされたとき
        if (isGameOver && Input.GetMouseButton(0))
        {
            Restart();
        }
    }

    // ユニティちゃんとの当たり判定
    // NOTE 物理的に接触するオブジェクト(isTrigger==true)同士の当たり判定用
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == player.name)
        {
            // ゲームオーバーを表示する
            text.GetComponent<Text>().text = "GameOver...";
            text.SetActive(true);

            // ユニティちゃんを動けなくする
            player.GetComponent<UnityChanControlScriptWithRgidBody>().enabled = false;
            // アニメーションをオフにする
            player.GetComponent<Animator>().enabled = false;

            isGameOver = true;
        }
    }

    private void Restart()
    {
        // 現在のScene名を取得する
        Scene loadScene = SceneManager.GetActiveScene();
        // Sceneの読み直し
        SceneManager.LoadScene(loadScene.name);
    }
}
